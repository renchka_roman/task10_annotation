package com.epam.course.renchka.view;

@FunctionalInterface
public interface Printable {

  void print();
}

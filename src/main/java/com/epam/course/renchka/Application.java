package com.epam.course.renchka;

import com.epam.course.renchka.view.MyView;
import java.io.IOException;

public class Application {

  public static void main(String[] args) throws IOException {
    MyView view = new MyView();
    view.start();
  }
}
